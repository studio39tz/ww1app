//imports
var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    fs = require('fs'),
    chalk = require('chalk'),
    jsonfile = require('jsonfile'),
    hbs = require('hbs'),
    bodyParser = require('body-parser'),
    multer = require('multer');

//middleware setup
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(multer());

//handlebars
app.set('view engine', 'html');
app.engine('html', hbs.__express);
hbs.registerPartials(__dirname + '/views');


//source folder
var source = {
    root: './public'
};

//routing

app.get('/', function (req, res) {
    jsonfile.readFile('./data/diggers.json', function(error, data) {
        if (error) {
            console.log(error);
        }
        res.render('index', data);
    });
});

//serve static files
app.use(express.static(__dirname + '/public'));
app.use('/scripts', express.static(__dirname + '/node_modules/bootstrap/dist/'));

//init http server
var server = http.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log(chalk.bold.green(' *** WW1 Server *** '));
    console.log(chalk.italic.yellow('listening on: { host: ' + host + ' port: ' + port + ' } '));
});
