///////////////////////////////////////////////////////
///					Lest We Forg///
///////////////////////////////////////////////////////

This app is to show stories of Diggers who went to WW1
in an innovative way. 

Data Used: 
Individual Digger data.
Protrait data
And National Archive data.

How the app works:
In the images folder you can see screen shots of the
prototype created.
The goal is to understand and humanise stories of Diggers
in WW1, so users can swipe through and read stories of 
where they went.