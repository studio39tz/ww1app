$(function () {
	var folderCount = $('#loaderSection section').length;
	var lastScrollTop = 0;


	var MOUSE_OVER = false;
	$('body').bind('mousewheel', function (e) {
		if (MOUSE_OVER) {
			if (e.preventDefault) {
				e.preventDefault();
			}
			e.returnValue = false;
			return false;
		}
	});

	$('#myDiv').mouseenter(function () {
		MOUSE_OVER = true;
	});
	$('#myDiv').mouseleave(function () {
		MOUSE_OVER = false;
	});
    
    setTimeout(function() {
        for (var i = 0; i < folderCount; i++) {
            $("#" + i).animate({
                top: "-1950"
            }, "slow");
        }
    }, 1000);

	$('body').bind('mousewheel', function (e) {
		var delta = e.wheelDelta;
		if (delta > 0) {
			//go up
		} else {
			for (var i = 0; i < folderCount; i++) {
				$("#" + i).animate({
					top: "-1950"
				}, "slow");
			}
		}
	});
	// map style

	var mapStyle = [{
		"featureType": "administrative",
		"elementType": "labels.text.fill",
		"stylers": [{
			"color": "#444444"
        }]
    }, {
		"featureType": "landscape",
		"elementType": "all",
		"stylers": [{
			"color": "#f2f2f2"
        }]
    }, {
		"featureType": "landscape",
		"elementType": "geometry.fill",
		"stylers": [{
			"color": "#f0e3ce"
        }, {
			"lightness": "39"
        }, {
			"gamma": "1.01"
        }]
    }, {
		"featureType": "poi",
		"elementType": "all",
		"stylers": [{
			"visibility": "off"
        }]
    }, {
		"featureType": "road",
		"elementType": "all",
		"stylers": [{
			"saturation": -100
        }, {
			"lightness": 45
        }]
    }, {
		"featureType": "road",
		"elementType": "geometry",
		"stylers": [{
			"color": "#ffffff"
        }]
    }, {
		"featureType": "road.highway",
		"elementType": "all",
		"stylers": [{
			"visibility": "simplified"
        }]
    }, {
		"featureType": "road.arterial",
		"elementType": "labels.icon",
		"stylers": [{
			"visibility": "off"
        }]
    }, {
		"featureType": "transit",
		"elementType": "all",
		"stylers": [{
			"visibility": "off"
        }]
    }, {
		"featureType": "water",
		"elementType": "all",
		"stylers": [{
			"color": "#e7e1d6"
        }, {
			"visibility": "on"
        }]
    }, {
		"featureType": "water",
		"elementType": "geometry",
		"stylers": [{
			"color": "#d9d9d9"
        }]
    }];

	var styledMap = new google.maps.StyledMapType(mapStyle, {
		name: "Styled Map"
	});

	//google maps
	function initialize(id, lat, long, title) {
		var mapOptions = {
			center: {
				lat: lat,
				lng: long
			},
			zoom: 10,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
			}
		};
		var map = new google.maps.Map(document.getElementById(id), mapOptions);
		map.mapTypes.set('map_style', styledMap);
		map.setMapTypeId('map_style');
        
        var marker = new google.maps.Marker({
            position: {
                lat: lat,
                lng: long
            },
            map: map,
            title: title
        });
	}


	var position = 0;
	for (var i = 0; i < folderCount; i++) {
		$('#' + i).height("80%");
		$('#' + i).turn({
			autoCenter: true,
			when: {
				turning: function (event, page, pageObj) {
					var folder = $('#' + position);
					if (folder) {
						var mapper = folder.find('.map');
						if (mapper) {
							for (var j in mapper) {
								if (mapper[j].id) {
									var ele = document.querySelector('#' + mapper[j].id);
									var lat = parseFloat(ele.dataset.lat);
									var long = parseFloat(ele.dataset.long);
									var title = ele.dataset.title;
									google.maps.event.addDomListener(window, 'load', initialize(mapper[j].id, lat, long, title));
								}
							}
						}
					}
				}
			}
		});
		$('#' + i).css({
			"position": "absolute",
			"top": $('#' + i).height * i + "+1500px",
			"left": "+10" * i,
			"z-index": "100" - i
		});
		if (i != position) {
			$('#' + i).turn("disable", true);
		}
		$('#' + i).bind('turning', function (event, page, view) {
			console.log(page);
			console.log(view);
			console.log($(this).turn('pages'));
			$('.backbg').css({
				"display": "block"
			});
			if (page < $(this).turn('pages')) {
				$('.backbg').addClass('fixed');
			} else
				$('.backbg').removeClass('fixed');

			if (page > 1)
				$('.frontback').addClass('fixed');
			else
				$('.frontback').removeClass('fixed');
		});


	}
	$(document).on('click', '#leftClick', function () {
		if (position < folderCount - 1) {
			for (var i = 0; i < folderCount; i++) {
				if (i != position + 1) {
					$('#' + i).turn('disable', true);
				} else if (i == position + 1)
					$('#' + i).turn('disable', false);
			}
			$("#" + position).animate({
				left: "-1000"
			}, "slow");
			position++;
		}
	});

	$(document).on('click', '#rightClick', function () {
		if (position > 0) {
			position--;
			for (var i = 0; i < folderCount; i++) {
				if (i != position) {
					$('#' + i).turn('disable', true);
				} else
					$('#' + i).turn('disable', false);
			}
			$("#" + position).animate({
				left: "0" + "10" * position
			}, "slow");
		}
	});



});