$(function () {

	var socket = io();

	$(document).on('click', '#submit', function (event) {
		console.log($("#key").val());
		//socket.emit('addinfo', data);

		var values = {
			"name": $("#name").val(),
			"key": $("#key").val(),
			"value": $("#value").val()
		};
		socket.emit('addinfo', values);

	});

	socket.on('display', function (data) {
		console.log(data);
	});
});