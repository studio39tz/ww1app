module.exports = function (io, jsonfile) {

	io.on('connection', function (socket) {
		socket.on('addinfo', function (data) {



			// var json2 = JSON.stringify(data);  //for sending the whole data
			var name = data.name;
			var key = data.key;
			var value = data.value;

			var json = JSON.stringify({
				key: key,
				value: value
			});
			console.log(json);
			var input = {
				"name": name,
				"data": [json]
			};
			console.log(input);
			var file = "./data/data.json";
			jsonfile.readFile(file, function (err, obj) {
				for (var i = 0; i <= obj.data.length; i++) {
					console.log(i);
					if (obj.data[0] == null) {
						obj.data.push(input);
						break;
					} else if (obj.data[i].name == name) {
						obj.data[i].data.push(json);
						break;
					} else if (obj.data[obj.data.length - 1].name != name && i == obj.data.length - 1) {
						obj.data.push(input);
						break;
					}
				}
				console.log(obj.data);
				socket.emit('display', obj);
				jsonfile.writeFile(file, obj, function (err) {
					//console.error(err)
				});
			});
		});
	});
};